ARG BASE_IMAGE=rust:latest
FROM $BASE_IMAGE

ARG SCCACHE=https://github.com/mozilla/sccache/releases/download/v0.2.15/sccache-v0.2.15-x86_64-unknown-linux-musl.tar.gz

RUN rustc --version

ENV CARGO_HOME=/opt/.cargo

RUN mkdir -p /opt/.cargo

RUN rustup component add rustfmt clippy 

RUN cargo install --force cargo-strip;\
    cargo install --force cargo-audit;\
    cargo install --force cargo-fuzz;\
    mv /opt/.cargo/bin/* /usr/bin/;\
    rm -rf ~/opt/.cargo/*

RUN export DEBIAN_FRONTEND=noninteractive; \
    apt-get update; \
    apt-get -y upgrade; \
    apt-get -y install --no-install-recommends lld cmake zstd; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*

# Install sccache from binary
RUN mkdir -p /tmp-sccache;\
    pushd /tmp-sccache;\
    wget $SCCACHE;\
    tar -xf sccache*;\
    rm *.tar.gz;\
    cd sccache*;\
    mv sccache /usr/bin/sccache;\
    chmod a+x /usr/bin/sccache;\
    popd;\
    rm -rf /tmp-sccache


COPY cargo.config /opt/.cargo/config
